import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import VueRouter from 'vue-router'
import VueAnalytics from 'vue-analytics'

import App from './App.vue'
import HomePage from './components/HomePage.vue'
import About from './components/About.vue'
import Map from './components/Map.vue'
import Frogs from './components/Frogs.vue'
import FrogProfile from './components/FrogProfile.vue'
import NotFound from './components/NotFound.vue'

Vue.use(VueRouter)

Vue.config.productionTip = false

var router = new VueRouter({
  history: true,
  routes: [
    { path: '/', name: 'home', component: HomePage },
    { path: '/about', name: 'about', component: About },
    { path: '/map', name: 'map', component: Map },
    { path: '/frogs', name: 'frogs', component: Frogs },
    { path: '/frogs/:id', name: 'frogs', component: FrogProfile },
    { path: '*', name: 'not-found', component: NotFound }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
});

Vue.use(VueAnalytics, {
  id: 'UA-67520440-2',
  router,
  debug: {
    sendHitTask: process.env.NODE_ENV === 'production'
  }
})

new Vue({
  router: router,
  render: createEle => createEle(App)
}).$mount('#app')


